package com.booking.service;

import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;
import com.booking.repositories.PersonRepository;
import com.booking.repositories.ServiceRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MenuService {
    private static final List<Person> personList = PersonRepository.getAllPerson();
    private static final List<Service> serviceList = ServiceRepository.getAllService();
    private static final List<Reservation> reservationList = new ArrayList<>();
    private static final Scanner input = new Scanner(System.in);

    public static void mainMenu() {
        String[] mainMenuArr = {"Show Data", "Create Reservation", "Complete/cancel reservation", "Exit"};
        String[] subMenuArr = {"Recent Reservation", "Show Customer", "Show Available Employee", "Show Reservation History", "Back to main menu"};

        boolean backToMainMenu = false;
        boolean backToSubMenu;
        do {
            PrintService.printMenu("Main Menu", mainMenuArr);
            int optionMainMenu = ValidationService.getValidatedNumberInput(input, "\\b[0-3]\\b", "Hanya bisa menerima input berupa angka dari 0-3");
            switch (optionMainMenu) {
                case 1:
                    backToSubMenu = false;
                    do {
                        PrintService.printMenu("Show Data", subMenuArr);
                        int optionSubMenu = ValidationService.getValidatedNumberInput(input, "\\b[0-4]\\b", "Hanya bisa menerima input berupa angka dari 0-4");
                        switch (optionSubMenu) {
                            case 1:
                                PrintService.showRecentReservation(reservationList);
                                break;
                            case 2:
                                PrintService.showAllCustomer(personList);
                                break;
                            case 3:
                                PrintService.showAllEmployee(personList);
                                break;
                            case 4:
                                PrintService.showHistoryReservation(reservationList);
                                break;
                            case 0:
                                backToSubMenu = true;
                                break;
                        }
                    } while (!backToSubMenu);
                    break;
                case 2:
                    ReservationService.createReservation(reservationList, personList, serviceList, input);
                    break;
                case 3:
                    ReservationService.editReservationWorkstage(reservationList, input);
                    break;
                case 0:
                    backToMainMenu = true;
                    System.out.println("Close");
                    break;
            }
        } while (!backToMainMenu);
    }
}
