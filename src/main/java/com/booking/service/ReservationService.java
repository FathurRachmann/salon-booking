package com.booking.service;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;

import java.util.List;
import java.util.Scanner;
import java.util.ArrayList;

public class ReservationService {

    public static void createReservation(List<Reservation> listReservations, List<Person> personList, List<Service> serviceList, Scanner input) {
        Customer customer = null;
        Employee employee = null;
        List<Service> orderedServices = new ArrayList<>();

        PrintService.showAllCustomer(personList);

        while (true) {
            System.out.println("Masukkan customer id di bawah ini: ");
            String customerId = input.nextLine();

            try {
                customer = getCustomerByCustomerId(personList, customerId);
                break;
            } catch (Exception e) {
                System.out.println("Data customer tidak ditemukan! Silakan masukkan kembali id customer yang benar");
            }
        }

        PrintService.showAllEmployee(personList);

        while (true) {
            System.out.println("Masukkan id pegawai di bawah ini: ");
            String employeeId = input.nextLine();

            try {
                employee = getEmployeeByEmployeeId(personList, employeeId);
                break;
            } catch (Exception e) {
                System.out.println("Data pegawai tidak ditemukan! Silakan masukkan kembali id pegawai yang benar");
            }
        }

        PrintService.showAllServices(serviceList);
        while (true) {
            System.out.println("Masukkan service id di bawah ini: ");
            String serviceId = input.nextLine();

            try {
                Service service = getServiceByServiceId(serviceList, serviceId);
                if (checkAddedService(orderedServices, serviceId)) {
                    System.out.println("Service sudah ditambahkan. Silakan masukkan service id yang lain!");
                } else {
                    orderedServices.add(service);
                    System.out.println("Ingin pilih service yang lain? (Y/N)");
                    String userInput = ValidationService.getValidatedStringInput(input, "\\b[YyNn]\\b", "Hanya bisa menerima input berupa huruf y dan n (kapital atau kecil)");

                    if (userInput.equalsIgnoreCase("n")) {
                        break;
                    }
                }

            } catch (Exception e) {
                System.out.println("Service tidak ditemukan! Silakan masukkan kembali id service yang benar");
            }
        }

        Reservation newReservation = new Reservation(generateReservationId(listReservations), customer, employee, orderedServices, "in process");

        listReservations.add(newReservation);

        System.out.println("Booking berhasil!");
        System.out.println("Total biaya booking: " + PrintService.formatCurrency(newReservation.calculateReservationPrice()));
    }

    private static Customer getCustomerByCustomerId(List<Person> personList, String customerId) {
        return (Customer) personList.stream()
                .filter(person -> person instanceof Customer)
                .filter(customer -> customer.getId().equalsIgnoreCase(customerId))
                .findFirst()
                .orElseThrow();
    }

    private static Employee getEmployeeByEmployeeId(List<Person> personList, String employeeId) {
        return (Employee) personList.stream()
                .filter(person -> person instanceof Employee)
                .filter(employee -> employee.getId().equalsIgnoreCase(employeeId))
                .findFirst()
                .orElseThrow();
    }

    private static Service getServiceByServiceId(List<Service> listServices, String serviceId) {
        return listServices.stream()
                .filter(service -> service.getServiceId().equalsIgnoreCase(serviceId))
                .findFirst()
                .orElseThrow();
    }

    private static Reservation getReservationByReservationId(List<Reservation> reservationList, String reservationId) {
        return reservationList.stream()
                .filter(reservation -> ((String) reservation.getReservationId()).equalsIgnoreCase(reservationId))
                .findFirst()
                .orElseThrow();
    }

    public static void editReservationWorkstage(List<Reservation> reservationList, Scanner input) {
        PrintService.showRecentReservation(reservationList);
        while (true) {
            System.out.println("Masukkan id reservasi: ");
            String reservationId = input.nextLine();

            try {
                Reservation updatedReservation = getReservationByReservationId(reservationList, reservationId);
                if (!updatedReservation.getWorkstage().equalsIgnoreCase("in process")) {
                    System.out.println("Reservasi yang dicari sudah selesai");
                } else {
                    System.out.println("Selesaikan reservasi: ");
                    String newWorkstage = ValidationService.getValidatedStringInput(input, "^(cancel|finish)$", "Hanya bisa menerima input \"cancel\" dan \"finish\" ");
                    updatedReservation.setWorkstage(newWorkstage);
                    reservationList.stream()
                            .map(reservation -> ((String) reservation.getReservationId()).equalsIgnoreCase(reservationId) ? updatedReservation : reservation);
                    System.out.println("Status reservasi dengan id " + reservationId + " diubah menjadi " + newWorkstage);
                    break;
                }
            } catch (Exception e) {
                System.out.println("Reservasi tidak ditemukan. Silakan masukan kembali id reservasi yang benar!");
            }
        }
    }

    private static String generateReservationId(List<Reservation> reservationList) {
        String result = "Rsv-";

        if (reservationList.isEmpty()) {
            result = "Rsv-1";
            return result;
        }

        Reservation lastAddedReservation = reservationList.get(reservationList.size() - 1);
        int numberOfLastAddedReservation = Integer.parseInt(((String) lastAddedReservation.getReservationId()).substring(4));
        int newNumber = numberOfLastAddedReservation + 1;

        return result + newNumber;
    }

    private static boolean checkAddedService(List<Service> orderedServices, String serviceId) {
        return orderedServices.stream()
                .anyMatch(service -> service.getServiceId().equalsIgnoreCase(serviceId));
    }
}
