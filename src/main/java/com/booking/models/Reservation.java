package com.booking.models;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Reservation {
    private String reservationId;
    private Customer customer;
    private Employee employee;
    private List<Service> services;
    private double reservationPrice;
    private String workstage;

    public Reservation(String reservationId, Customer customer, Employee employee, List<Service> services,
                       String workstage) {
        this.reservationId = reservationId;
        this.customer = customer;
        this.employee = employee;
        this.services = services;
        this.reservationPrice = calculateReservationPrice();
        this.workstage = workstage;
    }

    public double calculateReservationPrice() {
        double total = 0;
        for (Service service : services) {
            total += service.getPrice();
        }
        return total;
    }

    // Implementasi metode yang sebelumnya belum diimplementasi
    public String getWorkstage() {
        return workstage;
    }

    public String getReservationId() {
        return reservationId;
    }

    public List<Service> getServices() {
        return services;
    }

    public Customer getCustomer() {
        return customer;
    }

    public Employee getEmployee() {
        return employee;
    }
}
