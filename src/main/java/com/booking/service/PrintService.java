package com.booking.service;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;

import java.util.List;

public class PrintService {

    public static void printMenu(String title, String[] menuArr) {
        int num = 1;
        System.out.println(title);
        for (String menuItem : menuArr) {
            System.out.println(num + ". " + menuItem);
            num++;
        }
    }

    public static String printServices(List<Service> serviceList) {
        StringBuilder result = new StringBuilder();
        for (Service service : serviceList) {
            result.append(service.getServiceName()).append(", ");
        }
        return result.toString();
    }

    public static void showAllServices(List<Service> serviceList) {
        System.out.println("-----------------------------------------------------------");
        System.out.println("                        DATA SERVICE                          ");
        System.out.println("-----------------------------------------------------------");
        System.out.printf("| %-4s | %-8s | %-20s | %-15s |\n", "No.", "SERV ID", "NAME", "HARGA");
        System.out.println("-----------------------------------------------------------");

        int num = 1;
        for (Service service : serviceList) {
            System.out.printf("| %-4s | %-8s | %-20s | %-15s |\n", num, service.getServiceId(), service.getServiceName(), formatCurrency(service.getPrice()));
            num++;
        }

        System.out.println("------------------------------------------------------------\n");
    }

    public static void showRecentReservation(List<Reservation> reservationList) {
        int num = 1;
        System.out.println("-------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("                                                        DATA RESERVASI                               ");
        System.out.println("-------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.printf("| %-3s | %-4s | %-11s | %-60s | %-15s | %-10s | %-10s |\n", "No.", "ID", "Nama Customer", "Service", "Biaya Service", "Pegawai", "Workstage");
        System.out.println("+=========================================================================================================================================+");
        for (Reservation reservation : reservationList) {
            if ("in process".equalsIgnoreCase(reservation.getWorkstage())) {
                Customer customer = reservation.getCustomer();
                Employee employee = reservation.getEmployee();
                if (customer != null && employee != null) {
                    System.out.printf("| %-3s | %-4s | %-11s | %-60s | %-15s | %-10s | %-10s |\n", num, reservation.getReservationId(), customer.getName(), printServices(reservation.getServices()), formatCurrency(reservation.calculateReservationPrice()), employee.getName(), reservation.getWorkstage());
                    num++;
                }
            }
        }
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------\n");
    }

    public static void showHistoryReservation(List<Reservation> reservationList) {
        int num = 1;
        System.out.println("-------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("                                                        DATA RESERVASI                               ");
        System.out.println("-------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.printf("| %-3s | %-4s | %-11s | %-60s | %-15s | %-10s | %-10s |\n", "No.", "ID", "Nama Customer", "Service", "Biaya Service", "Pegawai", "Workstage");
        System.out.println("+==========================================================================================================================================+");
        for (Reservation reservation : reservationList) {
            System.out.printf("| %-3s | %-4s | %-11s | %-60s | %-15s | %-10s | %-10s |\n", num, reservation.getReservationId(), reservation.getCustomer().getName(), printServices(reservation.getServices()), formatCurrency(reservation.calculateReservationPrice()), reservation.getEmployee().getName(), reservation.getWorkstage());
            num++;
        }
        System.out.println("-------------------------------------------------------------------------------------------------------------------------------------------\n");

        double totalOmzet = reservationList.stream().filter(reservation -> "finish".equalsIgnoreCase(reservation.getWorkstage())).mapToDouble(Reservation::calculateReservationPrice).sum();
        System.out.println("Total keuntungan: " + formatCurrency(totalOmzet));
    }

    public static void showAllCustomer(List<Person> personList) {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("                                    DATA CUSTOMER                                ");
        System.out.println("-------------------------------------------------------------------------------");
        System.out.printf("| %-8s | %-8s | %-10s | %-10s | %-10s | %-13s  |\n", "No.", "ID", "Nama", "Alamat", "Membership", "Uang");
        System.out.println("-------------------------------------------------------------------------------");

        int num = 1;
        for (Person person : personList) {
            if (person instanceof Customer) {
                Customer customer = (Customer) person;
                System.out.printf("| %-8s | %-8s | %-10s | %-10s | %-10s | %-13s |\n", num, customer.getId(), customer.getName(), customer.getAddress(), customer.getMember().getMembershipName(), formatCurrency(customer.getWallet()));
                num++;
            }
        }

        System.out.println("-------------------------------------------------------------------------------\n");
    }

    public static void showAllEmployee(List<Person> personList) {
        System.out.println("--------------------------------------------------------------");
        System.out.println("                        DATA PEGAWAI                            ");
        System.out.println("--------------------------------------------------------------");
        System.out.printf("| %-8s | %-8s | %-10s | %-10s | %-10s |\n", "No.", "ID", "Nama", "Alamat", "Pengalaman (Tahun)");
        System.out.println("--------------------------------------------------------------");

        int num = 1;
        for (Person person : personList) {
            if (person instanceof Employee) {
                Employee employee = (Employee) person;
                System.out.printf("| %-8s | %-8s | %-10s | %-10s | %-10s |\n", num, employee.getId(), employee.getName(), employee.getAddress(), employee.getExperience());
                num++;
            }
        }

        System.out.println("---------------------------------------------------------------\n");
    }

    public static String formatCurrency(double price) {
        return "Rp." + String.format("%,.2f", price);
    }
}
